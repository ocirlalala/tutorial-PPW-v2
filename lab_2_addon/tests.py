from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.
class Lab2AddonUnitTest(TestCase):

	def test_lab_2_addon_url_is_exist(self):
		response = Client().get('/lab-2-addon/')
		self.assertEqual(response.status_code, 200)

	def test_lab_2_addon_using_index_func(self):
		found = resolve('/lab-2-addon/')
		self.assertEqual(found.func, index)