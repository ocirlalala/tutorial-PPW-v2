from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

bio_dict = [{'subject':'Name', 'value':mhs_name},\
{'subject':'Birth Date', 'value': birth_date.strftime('%d %B %Y')},\
{'subject':'Sex', 'value': 'Male'}]
response = {}

# Create your views here.
def index(request):
	html = 'lab_2_addon/lab_2_addon.html'
	return render(request, html, response)