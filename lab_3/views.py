from django.shortcuts import render, redirect
from .models import Diary
from datetime import datetime
import pytz


diary_dict = {}
response = {}
# Create your views here.
def index(request):
	html = 'lab_3/todolist.html'
	return render(request, html, {'diary_dict' : diary_dict})

def add_activity(request):
	if request.method == 'POST':
		date = datetime.strptime(request.POST['date'], '%Y-%m-%dT%H:%M')
		Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
		return redirect('/lab-3/')