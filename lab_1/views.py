from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Rico Putra Pradana' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,1,21) #TODO Implement this, format (Year, Month, Date)
response = {}

# Create your views here.
def index(request):
	response['name'] = mhs_name
	response['age'] = calculate_age(birth_date.year)
	response['birth_date'] = 21
	response['birth_month'] = 'Januari'
	response['birth_year'] = 1998
	html = 'lab_1/index_lab1.html'
	return render(request, html, response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
