# Create your views here.
from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

landing_page_content = 'Welcome to MyBiodata'
response = {}
def index(request):
	response['name'] = mhs_name
	response['content'] = landing_page_content
	html = 'lab_2/lab_2.html'
	return render(request, html, response)