from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, landing_page_content, mhs_name
from django.http import HttpRequest

# Create your tests here.
class Lab2UnitTest(TestCase):

	def test_lab_2_url_is_exist(self):
		response = Client().get('/lab-2/')
		self.assertEqual(response.status_code, 200)

	def test_lab_2_using_index_func(self):
		found = resolve('/lab-2/')
		self.assertEqual(found.func, index)

	def test_lab_2_landing_page_content_min_18_digit(self):
		self.assertTrue(len(landing_page_content) >= 18)